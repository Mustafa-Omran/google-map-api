function myMap() {
  var myCenter = new google.maps.LatLng(30.1197986,31.537000300000045);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 5};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);

  var infowindow = new google.maps.InfoWindow({
    content: "Welcome :)"
  });
  infowindow.open(map,marker);
}